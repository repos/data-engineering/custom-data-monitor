<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

const BASE_SCHEMA_DIR = __DIR__ . '/secondary/jsonschema/analytics';

function schemaHasRef( array $schema, string $ref ): bool {
    $allOf = $schema['allOf'] ?? [];

    foreach ( $allOf as $entry ) {
        if ( !array_key_exists( '$ref', $entry ) ) {
            continue;
        }

        if ( str_starts_with( $entry['$ref'], $ref ) ) {
            return true;
        }
    }

    return false;
}

function schemaIsLegacyEventLogging( array $schema ): bool {
    return schemaHasRef( $schema, '/fragment/analytics/legacy/eventcapsule' );
}

function schemaIsMetricsPlatform( array $schema ): bool {
    return schemaHasRef( $schema, '/analytics/product_metrics' );
}

function schemaGetProperties( array $schema ): ?array {
    if ( array_key_exists( 'properties', $schema ) ) {
        return $schema['properties'];
    }

    foreach ( $schema['allOf'] as $entry ) {
        if ( array_key_exists( 'properties', $entry ) ) {
            return $entry['properties'];
        }
    }

    return null;
}

function schemaGetCustomDataCount( array $schema ): int {
    $properties = schemaGetProperties( $schema );

    if ( $properties === null ) {
        // ???
        return 0;
    }

    if ( schemaIsLegacyEventLogging( $schema ) ) {
        $properties = schemaGetProperties( $properties['event'] );

        if ( $properties === null ) {
            // ???
            return 0;
        }
    }

    return count( $properties );
}

$currentYamlFiles = new RecursiveDirectoryIterator( BASE_SCHEMA_DIR );
$currentYamlFiles = new RecursiveIteratorIterator( $currentYamlFiles );
$currentYamlFiles = new RegexIterator( $currentYamlFiles, '/current.yaml$/' );

// Calculate
$nSchemas = 0;
$nMetricsPlatformSchemas = 0;
$customDataCount = 0;

/** @var SplFileInfo $file */
foreach ($currentYamlFiles as $file ) {
    $schema = Yaml::parseFile( $file->getPathname() );

    ++$nSchemas;

    if ( schemaIsMetricsPlatform( $schema ) ) {
        ++$nMetricsPlatformSchemas;
    }

    $customDataCount += schemaGetCustomDataCount( $schema );
}

// Report
if ( $_SERVER['argc'] === 1 ) {
    echo "date,n_schemas,n_metrics_platform_schemas,avg_custom_data_per_schema\n";

    exit();
}

$date = $_SERVER['argv'][1];

echo join(
    ',',
    [
        $date,
        $nSchemas,
        $nMetricsPlatformSchemas,
        round( $customDataCount / $nSchemas, 2 ),
    ]
);
echo "\n";