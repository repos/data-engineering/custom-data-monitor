# Custom Data Monitor

A script that reports high-level metrics about the JSON Schema schemas in
[schemas/event/secondary][1].

[schemas/event/secondary][1] stores the JSON Schema schemas for validating events submitted by
analytics instrumentation. It also stores the Metrics Platform per-platform base schemas (herein "the MP base schemas")
and the Legacy EventLogging base schema.

[Data Products](https://www.mediawiki.org/wiki/Data_Platform_Engineering/Data_Products) wants to track the number of
high-level metrics about the schemas used for analytics instrumentation over time. Those high-level metrics are: 

1. The number of schemas
2. The number of schemas that include the MP base schemas
3. The average number of so-called "custom data" per schema

Here Data Products defines custom data as any property defined in the schema that isn't in either the MP base schemas or
the Legacy EventLogging base schema.

This script reports those high-level metrics for each commit to [schemas/event/secondary][1]
since 2024/07/01 as a CSV. Since graphs are more pleasant to look at, this script has a companion spreadsheet that the
report can be copied to copy the report into, [the Custom Data Monitor Report Google Sheet][2]. 

## Usage

Run

```shell
./main.sh
```

and copy the output into
[the Custom Data Monitor Report Google Sheet][2].

🎉 🥳 🎉

[1]: https://gitlab.wikimedia.org/repos/data-engineering/schemas-event-secondary
[2]: https://docs.google.com/spreadsheets/d/1VApnnTtos6aO-YpcWGk5So3SwotnDyfFa1t6wtjXF_c/edit#gid=0&range=A2