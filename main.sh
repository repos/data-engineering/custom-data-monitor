#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

git submodule init --quiet
git submodule update --quiet

cd secondary

git checkout --quiet master
git pull --quiet

php ../main.php

# Based on the following StackOverflow answer by Justin Howard: https://stackoverflow.com/a/26984151
while read -r rev; do
  git checkout --quiet "$rev"

  DATE=$(git log -1 --format="%as")

  php ../main.php "${DATE}"

done < <(git rev-list --no-merges --since="2023-07-01" --reverse HEAD)

git checkout --quiet HEAD
